# <a href="https://kistauri.dtroode.now.sh/">Kistauri</a>

<img
  align="right"
  src="https://kistauri.dtroode.now.sh/icons/icon-144x144.png"
  width="100px"
  height="100px"
  alt="Blog Logo">

Hello, I am **David Kistauri**.\
This is repository of my personal website with **[blog](https://kistauri.dtroode.now.sh/blog).**

You can

- **Review** my code
- Submit **best solutions** to me
- **Fork** repository
- Submit **changes in articles**

To run locally, clone, then `nmp install`, then `nmp dev`, then go to [https://localhost:8000](https://localhost:8000).

**All blog posts in [/src/pages/blog/all](https://github.com/dtroode/kistauri/blob/master/src/pages/blog/all/)**
