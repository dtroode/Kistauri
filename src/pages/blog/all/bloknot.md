---
layout: blog
title: "Спустя много лет: как документировать"
description: Заметки по дням. Как оценивать свой день и получать опыт отовсюду
path: /blog/all
date: 2019-07-18
hero: /img/bloknot.jpg
categories: advices
tags: ["текст", "продуктивность", "вс код"]
---

После прочтения больше 50 заметок об искусстве письма, на меня напало вдохновение и захотелось все документировать. [Настроил редактор](https://kistauri.dtroode.now.sh/minimalistic-vscode/) и начал писать. Три раза заводил дневник по дням и документировал происходящее, верил в пользу: оценивать день, смотреть на ошибки и бесполезные дела, — но через десять заметок переставал писать. Я думал, что происходит слишком мало или просто забывал писать. А потом...

Внимание, самый крутой, эффективный, полезный, действенный способ писать, эксклюзивно и только тут!

> Открываешь редактор, создаешь новый файл, пишешь:
> Отредактировал текст для сайта.\
> Прочитал две главы из книги "Модульные системы в графическом дизайне".\
> Посмеялся с мемов.\
> Прочитал пять глав Ководства Лебедева.\
> Сделал что-то — написал «Сделал что-то».

### Затраты на такое ведение заметок

| Время                            | Усилия                                            | Ресурсы                                                 |
| :------------------------------- | :------------------------------------------------ | :------------------------------------------------------ |
| 1/10                             | 1/10                                              | 1/10                                                    |
| Сделал — записал. Секундное дело | Переключился на редактор, написал строку, обратно | Файлы занимают мало места, потому что это простой текст |

Браузер открыт на втором рабочем столе, а на первом — остальное для работы: редактор кода, текстовый редактор, файлы. В конце дня в файл пишу последнюю строку и не закрываю редактор. Утром просыпаюсь, вижу редактор со вчерашней заметкой, создаю новую.

Файлы храню в формате `.txt`, поэтому они занимают максимум один килобайт. Базу бэкаплю, только что это сделал.

![Заметки. 16 июля 2019](/img/daily-notes.jpg "Заметки. 16 июля 2019")

<figcaption>Пример заметки за 16 июля 2019</figcaption>
